global k R01 R02 ap d rho_l rho_p V02 P0 gamma sigma Pg0 Ap omega;
c = 1;
k = 20; %��� ������ k, ��� ������ ��������� ����� ��������� � ��������
R01 = 1e-5; % ������ ���������
R02 = c*(1e-5); % ������ �������
ap = R02;
d = R01+R02+k*R01; %��������� �� ������ �������� �� ������ �������
rho_l = 1000; % ��������� ��������
rho_p = 0;%2000; % ��������� �������, rho1 �� ����������, �.�. ������������ ��.
V02 = (4/3)*pi*R02^3; %�����  ������� (� ������ ������ �� �� ��������)
P0 = 1e+5; %����������� �������� (�������� � ��������)
gamma = 1.4; %����������� ���������
sigma = 0.073;
Pg0 = P0+2*sigma/R01;
Ap = P0/2;
nu = 200e+3;
omega = 2*pi*nu;
NT = 5;
tmax = NT/nu;
Nt=5001;
Toutput=linspace(0, tmax, Nt);
time = Toutput*nu;

options = odeset('RelTol',1e-4);%,'AbsTol',[1e-4 1e-4 1e-4 1e-4 1e-4 1e-4]);
% [T1D,Y1D] = ode45(@system_rho_eq_0,Toutput,[R01 0 1 -1 0 d], options);%_d_dynamic
% [Alter_Todd_T,Alter_Todd_Y] = ode45(@Alter_Todd_system_rho_eq_0,Toutput,[R01 0 1 -1 0 d], options);

options = odeset('RelTol',1e-9);%,'AbsTol',[1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4 1e-4]);
 [EQ_1D_Gumerov_T,EQ_1D_Gumerov_Y] = ode45(@systemEQ1D_Gumerov, Toutput, [R01 R02 0 0 0 0 0 d], options); 
% [EQ_3D_Gumerov_T, EQ_3D_Gumerov_Y] = ode45(@systemEQ3D_Gumerov, Toutput, [R01 R02 0 0 [0 0 0] [0 0 0] [0 0 0] [d 0 0]], options); 


% [Todd_T, Todd_Y] = ode45(@systemEQ1D_d_dynamic,Toutput,[R01 0 0 0 0 d], options);
%options = odeset('RelTol',1e-4,'AbsTol',[1e-4 1e-4]); %������ �� �� ���, ����������� ������� ���������, ������� -> 0 � ����������� d
 [T,Y1] = ode45(@SimpleSystem,Toutput,[R01 0], options);
 [T,Y2] = ode45(@RayleighPlesset,Toutput,[R01 0], options);

 [Todd_T, Todd_Y] = ode45(@system_rho_eq_0, Toutput, [R01 R02 0 0 0 0 0 d], options); 
% [Todd_T, Todd_Y] = ode45(@big_d, Toutput, [R01 R02 0 0 0 0 0 d], options); 


saveflag = 0;
set(0,'DefaultAxesFontSize',14);

% figure;
% subplot(2, 2, 1);
% plot(time, Todd_Y(:,1), time, Todd_Y(:,2), time, Y(:, 1), time, EQ_1D_Gumerov_Y(:,1));
% legend('Todd R_b', 'Todd R_p', 'Rayleigh-Plesset R(t)', 'Gumerov 1D R_b');
% 
% subplot(2, 2, 2);
% plot(time, Todd_Y(:,3), time, Todd_Y(:,4), time, Y(:, 2));
% legend('Todd w_b', 'Todd w_p', 'Rayleigh�-Plesset dR/dt');
% 
% subplot(2, 2, 3);
% plot(time, Todd_Y(:,5), time, Todd_Y(:,6));
% legend('Todd U_b', 'Todd U_p');
% 
% subplot(2, 2, 4);
% plot(time, Todd_Y(:,7), time, Todd_Y(:,8));
% legend('Todd r_b', 'Todd r_p');



% figure;
% % plot(time, Todd_Y(:,1), '>', time, Todd_Y(:,2), time, Y(:, 1), time, EQ_1D_Gumerov_Y(:,1),  'LineWidth', 2);
% % legend('Todd R_b', 'Todd R_p', 'Rayleigh-Plesset R(t)', 'Gumerov R_b');
% plot(time, Todd_Y(:,1), '>', time, EQ_1D_Gumerov_Y(:,1), 'o', time, Y1(:, 1), time, Y2(:, 1), 'LineWidth', 2);
% legend('Todd R(t)', 'Gumerov R(t)', 'Rayleigh-Plesset R(t)', 'Rayleigh-Plesset(*) R(t)');

figure;
% % plot(time, Todd_Y(:,3), time, Todd_Y(:,4), time, Y(:, 2), EQ_1D_Gumerov_Y(:,1),  'LineWidth', 2);
% % legend('Todd w_b', 'Todd w_p', 'Rayleigh-Plesset dR/dt', 'Gumerov dR/dt');
plot(time, Todd_Y(:,3), '>', time, EQ_1D_Gumerov_Y(:,3), 'o', time, Y1(:, 2), time, Y2(:, 2),  'LineWidth', 2);
legend('Todd w', 'Gumerov w', 'Rayleigh-Plesset w', 'Rayleigh-Plesset(*) w');

% figure;
% plot(time, Todd_Y(:,5), time, Todd_Y(:,6), time, EQ_1D_Gumerov_Y(:,5), time, EQ_1D_Gumerov_Y(:,6), 'LineWidth', 2);
% legend('Todd U_b', 'Todd U_p', 'Gumerov U_b', 'Gumerov U_p');
% 
% figure;
% plot(time, Todd_Y(:,7), 'o', time, Todd_Y(:,8), 'o', time, EQ_1D_Gumerov_Y(:,7), time, EQ_1D_Gumerov_Y(:,8),   'LineWidth', 3);
% legend('Todd r_b',  'Todd r_p',  'Gumerov r_b', 'Gumerov r_p');








% figure;
% fig1 = plot(time, Y(:,1)/R01, time, Y1D(:,1)/R01, time, Y3D(:,1)/R01, time, Y6D(:,1)/R01);
% xlabel('t/T');
% ylabel('a_b/R01');
% legend('Rayleigh-Plesset R(t)', 'Todd ODE R(t)', 'Gumerov 1D', 'Gumerov 3D ODE R(t)');
% if saveflag ~= 0
%     saveas(gcf,'fig1.png');
% end;
% figure;
% plot(time, Y1D(:,2), time, Y3D(:,2), time, Y6D(:,2));
% legend('Todd ODE dR(t)','Gumerov 1D', 'Gumerov 3D ODE dR(t)');
% xlabel('t/T');
% ylabel('w_b');
% if saveflag ~= 0
%     saveas(gcf,'fig2.png');
% end;
% figure;
% plot(time, Y1D(:,3), time, Y3D(:,3), time, Y6D(:,3));
% legend('Todd ODE Ub(t)',  'Gumerov 1D','Gumerov 3D ODE Ub(t)');
% xlabel('t/T');
% ylabel('U_b');
% if saveflag ~= 0
%     saveas(gcf,'fig3.png');
% end;
% figure;
% plot(time, Y1D(:,4), time, Y3D(:,4), time, Y6D(:,6));
% legend('Todd ODE Up(t)', 'Gumerov 1D', 'Gumerov 3D ODE Up(t)');
% xlabel('t/T');
% ylabel('U_p');
% if saveflag ~= 0
%     saveas(gcf,'fig4.png');
% end;
% figure;
% plot(time, Y1D(:,5), time, Y3D(:,5), time, Y6D(:,9));
% legend('Todd ODE rb(t)', 'Gumerov 1D', 'Gumerov 3D ODE rb(t)');
% xlabel('t/T');
% ylabel('r_b');
% if saveflag ~= 0
%     saveas(gcf,'fig5.png');
% end;
% figure;
% plot(time, Y1D(:,6), time, Y3D(:,6), time, Y6D(:,12));
% legend('Todd ODE rp(t)', 'Gumerov 1D', 'Gumerov 3D ODE rp(t)');
% xlabel('t/T');
% ylabel('r_p');
% if saveflag ~= 0
%     saveas(gcf,'fig6.png');
% end;
% 
% figure;
% plot(time, abs(Y6D(:,6)), time, abs(Y6D(:,3)) );
% legend('abs U_p Gumerov 3D', 'abs U_b Gumerov 3D');
% if saveflag ~= 0
%     saveas(gcf,'fig7.png');
% end;

% figure;
% plot(time, abs(Y3D(:,4)), time, abs(Y3D(:,3)) );
% legend('abs U_p Gumerov 1D', 'abs U_b Gumerov 1D');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;
% figure;
% plot(time, abs(Y1D(:,4)), time, abs(Y1D(:,3)) );
% legend('abs U_p Todd 1D', 'abs U_b Todd 1D');
% if saveflag ~= 0
%     saveas(gcf,'fig9.png');
% end;


% figure;
% plot(time, abs(Y3D(:,4)), time, abs(Y3D(:,3)), time, abs(Y6D(:,6)), time, abs(Y6D(:,3)) );
% legend('abs U_p Gumerov 1D', 'abs U_b Gumerov 1D', 'abs U_p Gumerov 3D', 'abs U_b Gumerov 3D');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;
% 
% figure;
% plot(time, abs(Y6D(:,6)), time, abs(Y6D(:,3)), time, abs(Y1D(:,4)), time, abs(Y1D(:,3)) );
% legend('abs U_p Gumerov 3D', 'abs U_b Gumerov 3D', 'abs U_p Todd 1D', 'abs U_b Todd 1D');
% if saveflag ~= 0
%     saveas(gcf,'fig7.png');
% end;
% 
% figure;
% plot(time, abs(Y4(:,4)), time, abs(Y4(:,3)),        time, abs(Y6D(:,6)), time, abs(Y6D(:,3)) );
% legend('abs U_p 1D', 'abs U_b 1D', 'abs U_p Gumerov 3D', 'abs U_b Gumerov 3D');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;
% 
% figure;
% plot(time, abs(Y1D(:,4)), time, abs(Y1D(:,3)),    time, abs(Y4(:,4)), time, abs(Y4(:,3)) );
% legend('abs U_p Todd 1D', 'abs U_b Todd 1D',      'abs U_p 1D', 'abs U_b 1D');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;

% figure;
% plot(time, abs(Y1D(:,3)),   time, abs(Y4(:,3)),  time, abs(Alter_Todd_Y(:,3)), 'LineWidth',2 );
% legend('U = |U_1| = |U_2| Todd 1D (equation 5)',     'U = |U_1| = |U_2| N.A. 1D (equation 7)',     'U = |U_1| = |U_2| Alter Todd (equation 8)');
% if saveflag ~= 0
%     saveas(gcf,'fig8.png');
% end;