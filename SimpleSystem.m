function dy = SimpleSystem( t, y ) % ������� ��� ode45 (�� ����������� ����� ��������� � d)
%Rayleigh Plesset without Berkness forces
global R01 rho_l P0 gamma sigma Pg0 Ap omega;

R1 = y(1);
dR1 = y(2);

P1 = (Pg0*(R01/R1)^(3*gamma)+Ap*sin(omega*t)-2*sigma/R1);
x = ((P1 - P0)/rho_l - 3/2*dR1^2)/R1;

dy = zeros(2, 1);
dy(1) = dR1;
dy(2) = x;


end

